package models

import (
	"encoding/hex"

	"github.com/mitchellh/mapstructure"

	"gitlab.com/xxacc/maclookup/store"
)

type Registry []Entry

func GetRegistry(db store.DB, name string) (Registry, error) {
	rows, err := db.GetBucket(name)
	if err != nil {
		return nil, err
	}
	return newRegistryFromRows(rows)
}

func LookupAssignment(db store.DB, name string, assignment MacPrefix) (Entry, error) {
	return Entry{}, nil
}

func newRegistryFromRows(rows store.Rows) (reg Registry, err error) {
	for row := rows.Next(); row != nil; row = rows.Next() {
		e := Entry{}
		if err := row.Scan(&e); err != nil {
			return nil, err
		}
		reg = append(reg, e)
	}
	return reg, nil
}

func (r Registry) Add(db store.DB, name string) error {
	var mList []map[string]interface{}
	for _, e := range r {
		mList = append(mList, e.Map())
	}
	return db.SetBucket(name, "assignment", mList)
}

func (r Registry) Merge(o Registry) {
	r = append(r, o...)
}

type MacPrefix []byte

func NewMacPrefixFromHex(s string) (MacPrefix, error) {
	b, err := hex.DecodeString(s)
	return MacPrefix(b), err
}

type Entry struct {
	Assignment MacPrefix `json:"assignment"`
	Name       string    `json:"name"`
	Address    string    `json:"address"`
}

func (e Entry) Map() map[string]interface{} {
	return map[string]interface{}{
		"assignment": e.Assignment,
		"name":       e.Name,
		"address":    e.Address,
	}
}

func (e *Entry) ScanRow(m store.M) error {
	ms, err := mapstructure.NewDecoder(&mapstructure.DecoderConfig{
		TagName: "json",
		Result:  e,
	})
	if err != nil {
		return err
	}

	return ms.Decode(m)
}
