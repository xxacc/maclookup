package models

import (
	"testing"

	"github.com/stretchr/testify/assert"
	"gitlab.com/xxacc/maclookup/store"
)

type mockRow map[string]interface{}

func (m mockRow) Map() map[string]interface{} {
	return m
}

func (m mockRow) Scan(scnr store.RowScanner) error {
	return scnr.ScanRow(store.M(m))
}

type mockRows struct {
	rows []mockRow
	idx  int
}

func (m *mockRows) Next() store.Row {
	if len(m.rows) == 0 || m.idx >= len(m.rows) {
		return nil
	}
	row := m.rows[m.idx]
	m.idx++
	return row
}

type mockDB struct {
	rows *mockRows
}

func (m *mockDB) GetBucket(name string) (store.Rows, error) {
	return m.rows, nil
}

func (m *mockDB) GetEntry(buk string, key string) (store.Row, error) {
	return nil, nil
}

func TestGetRegistry(t *testing.T) {
	type args struct {
		db   store.DB
		name string
	}
	tests := []struct {
		name    string
		args    args
		want    Registry
		wantErr bool
	}{
		{"Empty", args{&mockDB{&mockRows{}}, ""}, nil, false},
		{"Valid", args{
			&mockDB{
				&mockRows{
					rows: []mockRow{
						mockRow{},
					},
				},
			},
			""},
			Registry{Entry{}},
			false},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got, err := GetRegistry(tt.args.db, tt.args.name)
			if assert.Equal(t, tt.wantErr, err != nil, "Unexpected error return") {
				assert.Equal(t, tt.want, got)
			}
		})
	}
}
