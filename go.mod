module gitlab.com/xxacc/maclookup

require (
	github.com/joho/godotenv v1.3.0
	github.com/mitchellh/mapstructure v1.1.2
	go.etcd.io/bbolt v1.3.2
)
