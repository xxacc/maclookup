package main

import (
	"errors"
	"flag"
	"log"

	"github.com/joho/godotenv"
	"gitlab.com/xxacc/maclookup/fetcher"
	"gitlab.com/xxacc/maclookup/store"
)

func main() {
	flag.Parse()
	db, err := store.NewBoltDB("reg.db")
	if err != nil {
		log.Fatal(err)
	}
	switch flag.Arg(0) {
	case "fetch":
		if err := fetch(db, flag.Args()[2:]...); err != nil {
			log.Fatal(err)
		}
	}
}

func getConfig() {
	godotenv.Load()

}

func fetch(db store.DB, uris ...string) error {
	var ftch fetcher.Fetcher
	switch flag.Arg(1) {
	case "file":
		ftch = fetcher.FileFetcher{Paths: uris}
	default:
		return errors.New("Unknown fetcher type")
	}
	reg, err := ftch.FetchRegistry()
	if err != nil {
		return err
	}
	for k, v := range reg {
		if err := v.Add(db, k); err != nil {
			return err
		}
	}
	return nil
}
