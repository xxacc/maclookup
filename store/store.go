package store

type M map[string]interface{}

type Mapper interface {
	Map() map[string]interface{}
}

type DB interface {
	SetBucket(name string, keykey string, ms []map[string]interface{}) error
	SetEntry(buk string, m map[string]interface{}) error
	GetBucket(name string) (Rows, error)
	GetEntry(buk string, key string) (Row, error)
}

type Rows interface {
	Next() Row
}

type Row interface {
	Mapper
	Scan(scanner RowScanner) error
}

type RowScanner interface {
	ScanRow(m M) error
}
