package store

import (
	"encoding/json"
	"fmt"

	"go.etcd.io/bbolt"
)

type BoltDB struct {
	db *bbolt.DB
}

func NewBoltDB(fpath string) (*BoltDB, error) {
	db, err := bbolt.Open(fpath, 0600, nil)
	if err != nil {
		return nil, err
	}
	return &BoltDB{
		db: db,
	}, nil
}

func (db *BoltDB) SetBucket(name string, keykey string, ms []map[string]interface{}) error {
	return db.db.Update(func(tx *bbolt.Tx) error {
		b, err := tx.CreateBucketIfNotExists([]byte(name))
		if err != nil {
			return err
		}
		for _, m := range ms {
			j, err := json.Marshal(m)
			if err != nil {
				return err
			}
			fmt.Println("Put: ", j)
			if err := b.Put(m[keykey].([]byte), j); err != nil {
				return err
			}
		}
		return nil
	})
}

func (db *BoltDB) SetEntry(buk string, m map[string]interface{}) error {
	return nil
}

func (db *BoltDB) GetBucket(name string) (Rows, error) {
	return nil, nil
}

func (db *BoltDB) GetEntry(buk string, key string) (Row, error) {
	return nil, nil
}
