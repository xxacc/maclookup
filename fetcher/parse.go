package fetcher

import (
	"encoding/csv"
	"fmt"
	"io"

	"gitlab.com/xxacc/maclookup/models"
)

type row []string

func (r row) RegistryEntry() (regEnt models.Entry, err error) {
	defer func() {
		if r := recover(); r != nil {
			err = fmt.Errorf("%v", r)
		}
	}()
	prefix, err := models.NewMacPrefixFromHex(r[1])
	if err != nil {
		return models.Entry{}, err
	}
	return models.Entry{
		Assignment: prefix,
		Name:       r[2],
		Address:    r[3],
	}, err
}

func parseRegistry(r io.Reader) (RegistryMap, error) {
	csvReader := csv.NewReader(r)
	reg := make(RegistryMap)
	for csvRow, err := csvReader.Read(); err == nil; csvRow, err = csvReader.Read() {
		if err != nil && err != io.EOF {
			return reg, err
		}
		r := row(csvRow)
		if r[0] != "Registry" {
			regEnt, err := r.RegistryEntry()
			if err != nil {
				return reg, err
			}
			reg[r[0]] = append(reg[r[0]], regEnt)
		}
	}
	return reg, nil
}
