package fetcher

import (
	"net/http"
	"time"
)

type Client struct {
	client *http.Client
	urls   []string
}

func NewClient() *Client {
	return &Client{
		client: &http.Client{
			Timeout: time.Second * 30,
		},
	}
}

func (c *Client) FetchRegistry() (RegistryMap, error) {
	reg := make(RegistryMap)
	for _, url := range c.urls {
		partialReg, err := c.fetch(url)
		if err != nil {
			return nil, err
		}
		for k, v := range reg {
			v.Merge(partialReg[k])
		}
	}
	return reg, nil
}

func (c *Client) fetch(url string) (RegistryMap, error) {
	resp, err := c.client.Get(url)
	defer resp.Body.Close()
	if err != nil {
		return nil, err
	}
	reg, err := parseRegistry(resp.Body)
	if err != nil {
		return nil, err
	}
	return reg, nil
}
