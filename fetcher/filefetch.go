package fetcher

import (
	"os"
)

type FileFetcher struct {
	Paths []string
}

func (f FileFetcher) FetchRegistry() (RegistryMap, error) {
	reg := make(RegistryMap)
	for _, path := range f.Paths {
		f, err := os.Open(path)
		if err != nil {
			return reg, err
		}
		partialReg, err := parseRegistry(f)
		if err != nil {
			return reg, err
		}
		for k, v := range reg {
			v.Merge(partialReg[k])
		}
	}
	return reg, nil
}
