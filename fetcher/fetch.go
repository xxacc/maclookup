package fetcher

import "gitlab.com/xxacc/maclookup/models"

type RegistryMap map[string]models.Registry

type Fetcher interface {
	FetchRegistry() (RegistryMap, error)
}
