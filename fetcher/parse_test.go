package fetcher

import (
	"strings"
	"testing"

	"github.com/stretchr/testify/assert"
	"gitlab.com/xxacc/maclookup/models"
)

func Test_parseRegistry(t *testing.T) {
	tests := []struct {
		name    string
		rows    []string
		want    RegistryMap
		wantErr bool
	}{
		{
			"ValidCID",
			[]string{
				"CID,8A34BC,Fiberworks AS,Ryensvingen 15 Oslo  NO 0680",
			},
			RegistryMap{
				"CID": models.Registry{
					models.Entry{
						Assignment: models.MacPrefix([]byte{0x8A, 0x34, 0xBC}),
						Name:       "Fiberworks AS",
						Address:    "Ryensvingen 15 Oslo  NO 0680",
					},
				},
			},
			false,
		},
		{
			"TooFewValues",
			[]string{
				"CID,8A34BC,Fiberworks AS",
			},
			RegistryMap{},
			true,
		},
		{
			"ValidCIDWithFirstLine",
			[]string{
				"Registry,Assignment,Organization Name,Organization Address",
				"CID,8A34BC,Fiberworks AS,Ryensvingen 15 Oslo  NO 0680",
			},
			RegistryMap{
				"CID": models.Registry{
					models.Entry{
						Assignment: models.MacPrefix([]byte{0x8A, 0x34, 0xBC}),
						Name:       "Fiberworks AS",
						Address:    "Ryensvingen 15 Oslo  NO 0680",
					},
				},
			},
			false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got, err := parseRegistry(strings.NewReader(strings.Join(tt.rows, "\n")))
			if assert.Equal(t, tt.wantErr, err != nil, "Unexpected error return") {
				assert.Equal(t, tt.want, got)
			}
		})
	}
}
