# EnvTmpl

## Installing

Go get

```shell
go get gitlab.com/xxacc/maclookup
```

## Usage

### Options

```shell
maclookup [options]
```

## Todo

- Add tests!

## Versioning

We use [SemVer](http://semver.org/) for versioning. For the versions available, see the [tags on this repository](https://gitlab.com/xxacc/maclookup/tags).

## License

This project is licensed under the MIT License - see the [LICENSE](LICENSE) file for details
